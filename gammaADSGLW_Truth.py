import tensorflow as tf

def R_Kpi_f(gamma, R_cab, r_K, r_Kpi, d_K, d_Kpi, r_pi, d_pi):
    num = 1 + r_K ** 2 * r_Kpi ** 2 + 2 * r_K * r_Kpi * tf.cos(d_K - d_Kpi) * tf.cos(gamma)
    denom = 1 + r_pi ** 2 * r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi - d_Kpi) * tf.cos(gamma)

    return R_cab * (num / denom)

def R_KK_f(gamma, R_cab, r_K, d_K, r_pi, d_pi):
    num = 1 + r_K ** 2 + 2 * r_K * tf.cos(d_K) * tf.cos(gamma)
    denom = 1 + r_pi ** 2 + 2 * r_pi * tf.cos(d_pi) * tf.cos(gamma)

    return R_cab * (num / denom)

def R_pipi_f(gamma, R_cab, r_K, d_K, r_pi, d_pi):
    return R_KK_f(gamma, R_cab, r_K, d_K, r_pi, d_pi)

def A_Dpi_Kpi_fav_f(gamma, r_pi, r_Kpi, d_pi, d_Kpi):
    num = 2 * r_pi * r_Kpi * tf.sin(d_pi - d_Kpi) * tf.sin(gamma)
    denom = 1 + r_pi ** 2 * r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi - d_Kpi) * tf.cos(gamma)

    return num / denom

def A_DK_Kpi_fav_f(gamma, r_K, r_Kpi, d_K, d_Kpi):
    num = 2 * r_K * r_Kpi * tf.sin(d_K - d_Kpi) * tf.sin(gamma)
    denom = 1 + r_K ** 2 * r_Kpi ** 2 + 2 * r_K * r_Kpi * tf.cos(d_K - d_Kpi) * tf.cos(gamma)

    return num / denom

def A_DK_KK_CP_f(gamma, r_K, d_K, A_CP_KK):
    num = 2 * r_K * tf.sin(d_K) * tf.sin(gamma)
    denom = 1 + r_K ** 2 + 2 * r_K * tf.cos(d_K) * tf.cos(gamma)

    return (num / denom) + A_CP_KK

def A_DK_pipi_CP_f(gamma, r_K, d_K, A_CP_pipi):
    num = 2 * r_K * tf.sin(d_K) * tf.sin(gamma)
    denom = 1 + r_K ** 2 + 2 * r_K * tf.cos(d_K) * tf.cos(gamma)

    return (num / denom) + A_CP_pipi

def A_Dpi_KK_CP_f(gamma, r_pi, d_pi, A_CP_KK):
    num = 2 * r_pi * tf.sin(d_pi) * tf.sin(gamma)
    denom = 1 + r_pi ** 2 + 2 * r_pi * tf.cos(d_pi) * tf.cos(gamma)

    return (num / denom) + A_CP_KK

def A_Dpi_pipi_CP_f(gamma, r_pi, d_pi, A_CP_pipi):
    num = 2 * r_pi * tf.sin(d_pi) * tf.sin(gamma)
    denom = 1 + r_pi ** 2 + 2 * r_pi * tf.cos(d_pi) * tf.cos(gamma)

    return (num / denom) + A_CP_pipi

def R_DK_Kpi_m_f(gamma, r_K, r_Kpi, d_K, d_Kpi):
    num = r_K ** 2 + r_Kpi ** 2 + 2 * r_K * r_Kpi * tf.cos(d_K + d_Kpi - gamma)
    denom = 1 + r_K ** 2 * r_Kpi ** 2 + 2 * r_K * r_Kpi * tf.cos(d_K - d_Kpi - gamma)

    return num / denom

def R_DK_Kpi_p_f(gamma, r_K, r_Kpi, d_K, d_Kpi):
    num = r_K ** 2 + r_Kpi ** 2 + 2 * r_K * r_Kpi * tf.cos(d_K + d_Kpi + gamma)
    denom = 1 + r_K ** 2 * r_Kpi ** 2 * r_K * r_Kpi * tf.cos(d_K - d_Kpi + gamma)

    return num / denom

def R_Dpi_Kpi_m_f(gamma, r_pi, r_Kpi, d_pi, d_Kpi):
    num = r_pi ** 2 + r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi + d_Kpi - gamma)
    denom  = 1 + r_pi ** 2 * r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi - d_Kpi - gamma)

    return num / denom

def R_Dpi_Kpi_p_f(gamma, r_pi, r_Kpi, d_pi, d_Kpi):
    num = r_pi ** 2 + r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi + d_Kpi + gamma)
    denom = 1 + r_pi ** 2 * r_Kpi ** 2 + 2 * r_pi * r_Kpi * tf.cos(d_pi - d_Kpi + gamma)

    return num / denom
