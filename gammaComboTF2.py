import matplotlib.pyplot as plt

from matplotlib import rcParams
import matplotlib as mpl
mpl.use('Agg')

plt.style.use(['seaborn-whitegrid', 'seaborn-ticks'])
import matplotlib.ticker as plticker
rcParams['figure.figsize'] = 8, 6
rcParams['axes.facecolor'] = 'FFFFFF'
rcParams['savefig.facecolor'] = 'FFFFFF'
rcParams['figure.facecolor'] = 'FFFFFF'

rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'

rcParams['mathtext.fontset'] = 'cm'
rcParams['mathtext.rm'] = 'serif'

rcParams.update({'figure.autolayout': True})

import numpy as np

from tqdm import tqdm

import pickle

import h5py

from corner import corner

from statsmodels.stats.moment_helpers import corr2cov

import tensorflow as tf
import tensorflow_probability as tfp

import sys

import os

import time

tfd = tfp.distributions

import gammaADSGLW_Truth as adsglwFuncs

xD = (0.00410, 0.00150)
yD = (0.00630, 0.00080)
d_Kpi = (3.26900, 0.20071)
R_Kpi = (0.00349, 0.00004) # NOT little r
A_CP_pipi = (0.00140, 0.00150)
A_CP_KK = (-0.00150, 0.00140)

glwadsCharmParams = np.array([xD, yD, d_Kpi, R_Kpi, A_CP_pipi, A_CP_KK], dtype = np.float32)

glwadsCharmCorr = np.array([
[1.000, -0.270, -0.119, 0.352, 0.070, 0.079],
[-0.270, 1.000, 0.918, 0.119, -0.124, -0.137],
[-0.119, 0.918, 1.000, 0.404, -0.149, -0.162],
[0.352, 0.119, 0.404, 1.000, -0.026, -0.025],
[0.070, -0.124, -0.149, -0.026, 1.000, 0.716],
[0.079, -0.137, -0.162, -0.025, 0.716, 1.000]
], dtype = np.float32)

# No D mixing
glwadsCharmParams = glwadsCharmParams[2:]
glwadsCharmCorr = glwadsCharmCorr[2:,2:]

glwadsCharmCov = corr2cov(corr = glwadsCharmCorr, std = glwadsCharmParams[:,1])

x_m = np.array([2.5, 2.5, 1.0, 0.5], dtype = np.float32) * 1E-2
y_m = np.array([7.5, 2.9, 0.5, 1.4], dtype = np.float32) * 1E-2
x_p = np.array([-7.7, 2.4, 1.0, 0.4], dtype = np.float32) * 1E-2
y_p = np.array([-2.2, 2.5, 0.4, 1.0], dtype = np.float32) * 1E-2

ggszLHCbParams = np.array([x_m, y_m, x_p, y_p], dtype = np.float32)

ggszLHCbStatCorr = np.array([
[1., -0.247, 0.038, -0.003],
[-0.247, 1., -0.011, 0.012],
[0.038, -0.011, 1., 0.002],
[-0.003, 0.012, 0.002, 1.],
], dtype = np.float32)

ggszLHCbSystCorr = np.array([
[1., 0.005, -0.025, 0.070],
[0.005, 1., 0.009, -0.141],
[-0.025, 0.009, 1., 0.008],
[0.070, -0.141, 0.008, 1.],
], dtype = np.float32)

ggszLHCbExternCorr = np.array([
[1., 0., 0., 0.],
[0., 1., 0., 0.],
[0., 0., 1., 0.],
[0., 0., 0., 1.],
], dtype = np.float32)

ggszStatCov = corr2cov(corr = ggszLHCbStatCorr, std = np.array([x_m[1], y_m[1], x_p[1], y_p[1]]))
ggszSystCov = corr2cov(corr = ggszLHCbSystCorr, std = np.array([x_m[2], y_m[2], x_p[2], y_p[2]]))
ggszExternCov = corr2cov(corr = ggszLHCbExternCorr, std = np.array([x_m[3], y_m[3], x_p[3], y_p[3]]))

ggszTotalCov = ggszStatCov + ggszSystCov + ggszExternCov

R_Kpi = (0.0774 , 0.0012 , 0.0018)
R_KK = (0.0773 , 0.0030 , 0.0018)
R_pipi = (0.0803 , 0.0056 , 0.0017)
A_Dpi_Kpi_fav = (-0.0001 , 0.0036 , 0.0095)
A_DK_Kpi_fav  = (0.0044 , 0.0144 , 0.0174)
A_DK_KK_CP = (0.148 , 0.037 , 0.010)
A_DK_pipi_CP = (0.135 , 0.066 , 0.010)
A_Dpi_KK_CP = (-0.020 , 0.009 , 0.012)
A_Dpi_pipi_CP = (-0.001 , 0.017 , 0.010)
R_DK_Kpi_m = (0.0073 , 0.0023 , 0.0004)
R_DK_Kpi_p = (0.0232 , 0.0034 , 0.0007)
R_Dpi_Kpi_m = (0.00469 , 0.00038 , 0.00008)
R_Dpi_Kpi_p = (0.00352 , 0.00033 , 0.00007)

glwadsLHCbParams = np.array([R_Kpi, R_KK, R_pipi, A_Dpi_Kpi_fav,
                             A_DK_Kpi_fav, A_DK_KK_CP, A_DK_pipi_CP,
                             A_Dpi_KK_CP, A_Dpi_pipi_CP, R_DK_Kpi_m,
                             R_DK_Kpi_p, R_Dpi_Kpi_m, R_Dpi_Kpi_p], dtype = np.float32)

glwadsStatCorr = np.diag(np.ones(len(glwadsLHCbParams), dtype = np.float32))
glwadsSystCorr = np.diag(np.ones(len(glwadsLHCbParams), dtype = np.float32))

glwadsStatCov = corr2cov(corr = glwadsStatCorr, std = glwadsLHCbParams[:,1])
glwadsSystCov = corr2cov(corr = glwadsSystCorr, std = glwadsLHCbParams[:,2])

glwadsTotalCov = glwadsStatCov + glwadsSystCov

glwadsRobustLHCbParams = np.array([R_Kpi, R_KK, R_pipi,
                             A_DK_Kpi_fav, A_DK_KK_CP, A_DK_pipi_CP,
                             R_DK_Kpi_m, R_DK_Kpi_p], dtype = np.float32)

glwadsRobustStatCorr = np.diag(np.ones(len(glwadsRobustLHCbParams), dtype = np.float32))
glwadsRobustSystCorr = np.diag(np.ones(len(glwadsRobustLHCbParams), dtype = np.float32))

glwadsRobustStatCov = corr2cov(corr = glwadsRobustStatCorr, std = glwadsRobustLHCbParams[:,1])
glwadsRobustSystCov = corr2cov(corr = glwadsRobustSystCorr, std = glwadsRobustLHCbParams[:,2])

glwadsRobustTotalCov = glwadsRobustStatCov + glwadsRobustSystCov

def x_m_f(r_K, d_K, gamma):
    return r_K * tf.cos(d_K - gamma)

def y_m_f(r_K, d_K, gamma):
    return r_K * tf.sin(d_K - gamma)

def x_p_f(r_K, d_K, gamma):
    return r_K * tf.cos(d_K + gamma)

def y_p_f(r_K, d_K, gamma):
    return r_K * tf.sin(d_K + gamma)

from tensorflow_probability import edward2 as ed

MVN = ed.MultivariateNormalFullCovariance

def glawds_robust_ggsz_model():

    rv_r_K = ed.Uniform(low = 0.0, high = 1.0, name = 'rv_r_K')
    rv_d_K = ed.Uniform(low = 0.0, high = np.pi, name = 'rv_d_K')

    rv_r_pi = ed.Uniform(low = 0.0, high = 1.0, name = 'rv_r_pi')
    rv_d_pi = ed.Uniform(low = 0.0, high = np.pi, name = 'rv_d_pi')

    rv_R_cab = ed.Uniform(low = 0.0, high = 1.0, name = 'rv_R_cab')

    rv_gamma = ed.Uniform(low = 0.0, high = np.pi, name = 'rv_gamma')

    # GLW/ADS

    charmInputs = MVN(loc = glwadsCharmParams[:,0],
                      covariance_matrix = glwadsCharmCov,
                      name = 'charmInputs')

    # Indexing -> no D mixing
    rv_d_Kpi = charmInputs[0]
    rv_r_Kpi = tf.sqrt(charmInputs[1]) # big R -> little R
    rv_A_CP_pipi = charmInputs[2]
    rv_A_CP_KK = charmInputs[3]

    rv_R_Kpi = adsglwFuncs.R_Kpi_f(rv_gamma, rv_R_cab, rv_r_K, rv_r_Kpi, rv_d_K, rv_d_Kpi, rv_r_pi, rv_d_pi)
    rv_R_KK = adsglwFuncs.R_KK_f(rv_gamma, rv_R_cab, rv_r_K, rv_d_K, rv_r_pi, rv_d_pi)
    rv_R_pipi =  adsglwFuncs.R_pipi_f(rv_gamma, rv_R_cab, rv_r_K, rv_d_K, rv_r_pi, rv_d_pi)
    rv_A_DK_Kpi_fav = adsglwFuncs.A_DK_Kpi_fav_f(rv_gamma, rv_r_K, rv_r_Kpi, rv_d_K, rv_d_Kpi)
    rv_A_DK_KK_CP = adsglwFuncs.A_DK_KK_CP_f(rv_gamma, rv_r_K, rv_d_K, rv_A_CP_KK)
    rv_A_DK_pipi_CP = adsglwFuncs.A_DK_pipi_CP_f(rv_gamma, rv_r_K, rv_d_K, rv_A_CP_pipi)
    rv_R_DK_Kpi_m = adsglwFuncs.R_DK_Kpi_m_f(rv_gamma, rv_r_K, rv_r_Kpi, rv_d_K, rv_d_Kpi)
    rv_R_DK_Kpi_p = adsglwFuncs.R_DK_Kpi_p_f(rv_gamma, rv_r_K, rv_r_Kpi, rv_d_K, rv_d_Kpi)

    glwadsObs = MVN(loc = [rv_R_Kpi, rv_R_KK, rv_R_pipi,
                     rv_A_DK_Kpi_fav, rv_A_DK_KK_CP, rv_A_DK_pipi_CP,
                     rv_R_DK_Kpi_m, rv_R_DK_Kpi_p],
              covariance_matrix = glwadsRobustTotalCov,
              name = 'glwadsObs')

    # GGSZ

    rv_x_m = x_m_f(rv_r_K, rv_d_K, rv_gamma)
    rv_y_m = y_m_f(rv_r_K, rv_d_K, rv_gamma)
    rv_x_p = x_p_f(rv_r_K, rv_d_K, rv_gamma)
    rv_y_p = y_p_f(rv_r_K, rv_d_K, rv_gamma)

    ggszObs = MVN(loc = [rv_x_m, rv_y_m, rv_x_p, rv_y_p],
              covariance_matrix = ggszTotalCov,
              name = 'ggszObs')

log_joint_glawds_robust_ggsz = ed.make_log_joint_fn(glawds_robust_ggsz_model)

def target_log_prob_fn_glawds_robust_ggsz(r_K, d_K, r_pi, d_pi, R_cab, gamma):
    return log_joint_glawds_robust_ggsz(glwadsObs = glwadsRobustLHCbParams[:,0], charmInputs = glwadsCharmParams[:,0],
                     ggszObs = ggszLHCbParams[:,0],
                     rv_r_K = r_K, rv_d_K = d_K, rv_r_pi = r_pi, rv_d_pi = d_pi, rv_R_cab = R_cab, rv_gamma = gamma)

initial_chain_state = [
0.1 * tf.ones([], dtype=tf.float32, name="init_rv_r_K"),
1.0 * tf.ones([], dtype=tf.float32, name="init_rv_d_K"),
0.1 * tf.ones([], dtype=tf.float32, name="init_rv_r_pi"),
1.0 * tf.ones([], dtype=tf.float32, name="init_rv_d_pi"),
0.1 * tf.ones([], dtype=tf.float32, name="init_rv_R_cab"),
1.0 * tf.ones([], dtype=tf.float32, name="init_rv_gamma"),
]

num_results = int(1E5)
num_burnin_steps = 500000

@tf.function(autograph=False, experimental_compile=True)
def do_sampling():

  kernel = tfp.mcmc.HamiltonianMonteCarlo(
      target_log_prob_fn=target_log_prob_fn_glawds_robust_ggsz,
      step_size=0.01,
      num_leapfrog_steps=3)

  kernel = tfp.mcmc.SimpleStepSizeAdaptation(
    inner_kernel=kernel, num_adaptation_steps=int(num_burnin_steps * 0.8))

  return tfp.mcmc.sample_chain(
      num_results=num_results,
      num_burnin_steps=num_burnin_steps,
      current_state=initial_chain_state,
      kernel=kernel,
      trace_fn=lambda _, pkr: pkr.inner_results.is_accepted)

start = time.perf_counter()

# (r_K, d_K, r_pi, d_pi, R_cab, gamma), is_accepted = do_sampling()
chains, accepted = do_sampling()

end = time.perf_counter()

print(np.sum(accepted))
print("Elapsed time = {:.12f} seconds".format(end - start))

corner(np.array([chains[0][accepted],
      np.degrees(chains[1][accepted]),
      chains[2][accepted],
      np.degrees(chains[3][accepted]),
      chains[4][accepted],
      np.degrees(chains[5][accepted])]).T,
      labels = ['$r_K$', '$\delta_K$', '$r_\pi$', '$\delta_\pi$', '$R_{cab}$', '$\gamma$'],
      label_kwargs = {'fontsize' : 20}, bins = 50, plot_contours = True, smooth = 0.8);

plt.savefig('cornerGammaCombo.pdf')
